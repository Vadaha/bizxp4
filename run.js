var forever = require('forever-monitor');
//------------------------------------------------------------
var child = new (forever.Monitor)('./api.js',
{
'minUptime': 50, 
'spinSleepTime': 10,
'logFile': './log_log.txt', // Path to log output from forever process (when daemonized) 
'outFile': './out_log.txt', // Path to log output from child stdout 
'errFile': './err_log.txt' // Path to log output from child stderr 
});

  child.on('exit', function () {
    console.log('api.js has exited after 3 restarts');
  });

  child.start();