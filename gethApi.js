#!/usr/local/bin/node

var gethApi = (function(){
    var self = {}

    var web3_extended = require('web3_ipc');
    var shell = require('shelljs');
    var fs = require('fs');
    var sqlite3 = require('sqlite3').verbose();

    var LOCAL_IP = '';
    var GLOGAL_IP; // to be implemented
    var CURR_DIR = shell.pwd().toString();

    var options = {
      host: 'http://localhost:8545',
      ipc:false,
      personal: true,
      admin: true,
      debug: false,
      miner: true
    };
    var web3 = web3_extended.create(options);

    // Auto executing function, inits the database
    function init_sqlite(){
        var db = new sqlite3.Database('./database');
        db.serialize(function(){
            db.run("CREATE TABLE IF NOT EXISTS contracts2 (name TEXT, abi TEXT, address TEXT, timestamp INTEGER, accountNum INTEGER,receiver INTEGER)");
            db.close();
        });
    }
    function add_history_db(id, sender, reciver, contract,event, val, timestamp,txt){
        // Adds contract's keys to the database
        // Also need to add function calls, maybe in another table
        console.log('add_history_db');
        var db = new sqlite3.Database('./database');
        db.serialize(function(){
            var toInsert = db.prepare('INSERT INTO history (  `sender` , `reciver` , `contract` , `event` , `val` , `timestamp` , `text`  ) VALUES (?,?,?,?,?,?,?)');
            toInsert.run(  sender, reciver, contract, event, val, timestamp, txt);
            toInsert.finalize();
           
                db.close();
                console.log('add_history_db_DONE');
       
        });
    }
    function add_contract_db(name, abi, address, timestamp, accountNum, receiver,cache){
        // Adds contract's keys to the database
        // Also need to add function calls, maybe in another table
        var db = new sqlite3.Database('./database');
        console.log('Add to db');
        db.serialize(function(){
            var toInsert = db.prepare('INSERT INTO contracts2 VALUES (?,?,?,?,?,?)');
            toInsert.run(name, abi, address, timestamp, accountNum,receiver);
            toInsert.finalize();
            db.each('SELECT MAX(rowid) FROM contracts2',function(err,col){
                if (err) console.log("Query Error: "+err);
                cache.push(col['MAX(rowid)']);
                console.log('Saved');
                db.close();
            });
        });
    }
        function add_contract_db2(name, abi, address, timestamp, accountNum, receiver,cache, status, info, history){
        // Adds contract's keys to the database
        // Also need to add function calls, maybe in another table
        var db = new sqlite3.Database('./database');
        console.log('Add to db: '+ info);
        db.serialize(function(){
           
            var toInsert = db.prepare('INSERT INTO contracts2 VALUES (?,?,?,?,?,?,?,?,?)');
            toInsert.run(name, abi, address, timestamp, accountNum,receiver,status,info, history);
            toInsert.finalize();
            //db.close();
            db.each('SELECT MAX(rowid) FROM contracts2',function(err,col){
                if (err) console.log("Query Error: "+err);
                cache.push(col['MAX(rowid)']);
                console.log('Saved');
                db.close();
            });
        });
    }

    function delete_contract_db(name, abi, id){
        var db = new sqlite3.Database('./database');
        db.serialize(function(){
            var toDelete = db.prepare('DELETE FROM contracts2 WHERE name = ? AND abi = ? AND address = "0x0" AND rowid = ?');
            toDelete.run(name,abi,id);
            toDelete.finalize();
            console.log('Saved');
            db.close();
        });
    }

    function update_contract_db(address, timestamp, id){
        var db = new sqlite3.Database('./database');
        db.serialize(function(){
            var toUpdate = db.prepare('UPDATE contracts2 SET address = ?, timestamp = ? WHERE rowid = ?');
            toUpdate.run(address,timestamp, id);
            toUpdate.finalize();
            console.log('UPDATE Saved');
            db.close();
        });
    }

    self.read_top10_db = function(val){
        //  Gets 10 top rows in the database.
        var db = new sqlite3.Database('./database');
        db.serialize(function(){
            db.each('SELECT rowid,name,abi,address,timestamp,accountNum,receiver,stat, info, history FROM contracts2 ORDER BY rowid DESC LIMIT 20 ',function(err, row){
                 if (err) console.log("Query Error: "+err);
              //   console.log(' numb = ' + row.rowid + ' info field = ' + row.info);
                val.push([row.rowid,row.name,row.abi,row.address,row.timestamp,row.accountNum,row.receiver,row.stat,row.info,row.history]);
                // console.log(row.rowid,row.name,row.abi,row.address,row.timestamp);
            }, function (err, cntx) {
                if (err) console.log("Query Error: "+err);
              //  console.log('Number of retrieved rows is ' + cntx)
                
            });
            db.close();
        });
    }
        self.read_top50_db = function(val){
        //  Gets 10 top rows in the database.
        var db = new sqlite3.Database('./database');
        db.serialize(function(){
            db.each('SELECT rowid,name,abi,address,timestamp,accountNum,receiver FROM contracts ORDER BY rowid DESC LIMIT 50 ',function(err, row){
                val.push([row.rowid,row.name,row.abi,row.address,row.timestamp,row.accountNum,row.receiver]);
                // console.log(row.rowid,row.name,row.abi,row.address,row.timestamp);
            }, function(err, rows) {
  if (rows == 0) {
    console.log('No lines');
  }
});
            db.close();
        });
    }

    // ###########################################
    // Gets local ip address
    // Useful to connect nodes on the same machine

    var getNetworkIPs = (function() {
        var ignoreRE = /^(127\.0\.0\.1|::1|fe80(:1)?::1(%.*)?)$/i;

        var exec = require('child_process').exec;
        var cached;
        var command;
        var filterRE;

        switch (process.platform) {
        case 'win32':
        //case 'win64': // TODO: test
            command = 'ipconfig';
            filterRE = /\bIPv[46][^:\r\n]+:\s*([^\s]+)/g;
            break;
        case 'darwin':
            command = 'ifconfig';
            filterRE = /\binet\s+([^\s]+)/g;
            // filterRE = /\binet6\s+([^\s]+)/g; // IPv6
            break;
        default:
            command = 'ifconfig';
            filterRE = /\binet\b[^:]+:\s*([^\s]+)/g;
            // filterRE = /\binet6[^:]+:\s*([^\s]+)/g; // IPv6
            break;
        }

        return function (callback, bypassCache) {
            if (cached && !bypassCache) {
                callback(null, cached);
                return;
            }
            // system call
            exec(command, function (error, stdout, sterr) {
                cached = [];
                var ip;
                var matches = stdout.match(filterRE) || [];
                //if (!error) {
                for (var i = 0; i < matches.length; i++) {
                    ip = matches[i].replace(filterRE, '$1')
                    if (!ignoreRE.test(ip)) {
                        cached.push(ip);
                    }
                }
                //}
                callback(error, cached);
            });
        };
    })();

    // Async call
    getNetworkIPs(function (error, ip){
      LOCAL_IP = ip[0];
      if (error) {
        console.log('Local ip not found');
      }
    }, false);
    // ###########################################

    self.get_wei = function(account){
        //Get account's ether in wei
        return web3.eth.getBalance(account);
    }

    self.get_client_enode = function(ip){
        var res = web3.admin.nodeInfo().enode;
        res = res.replace('[::]', ip);
        return res;
    }

    self.get_peers = function(){
      return web3.admin.peers;
    }

    self.get_peers_count = function(){
      return web3.net.peerCount;
    }

    function mine(_switch, cores = 1){
      /* Mining switch.
      A contract has to be mined to get an address on the BC.
      This function will be used in deployment function */
      if (_switch === 'start'){
          web3.miner.start(cores);
          console.log('mining begun...');
      }
      else if (_switch === 'stop'){
          web3.miner.stop();
          console.log('mining stopped...')
      }
      else{
        console.log('ERROR: Incorrect argument in mining function')
      }
    }

    self.get_account = function(number = 0){
        return web3.eth.accounts[number];
    }

    self.deploy_sol = function(name, accountNum,receiver, ...params){
        fs.readFile(CURR_DIR+'/contracts/'+name+'.sol','utf8',function(err, data){
            if (err){
                console.log('readfile error',err);
                return false
            }
            var compiled = web3.eth.compile.solidity(data);
            console.log('SUCCESS: '+name+' compiled!');
            // Cache the ABI
            var abi = compiled[name]['info']['abiDefinition'];
            // Cache the Binary
            var bin = compiled[name]['code'];
            // unlock account: for the moment hardcoded and not secure
            web3.personal.unlockAccount(self.get_account(accountNum),'0000');
            // Create solidity contract object from abi
            var contract = web3.eth.contract(abi);
            // DEPLOYEMENT
            console.log('======')
            console.log('Deployement...');
            console.log('======')
                var datenow  = Math.floor(Date.now() / 1000);

                    var date = new Date();


var dateform = date.getFullYear() + '-' + ('0' + (date.getMonth() + 1)).slice(-2) + '-' + ('0' + date.getDate()).slice(-2)




            if(name === 'InvoiceContract'){
                web3.miner.setEtherbase(self.get_account(accountNum));
                var amount2pay = params[0];
                var product = params[1];
                var message = params[2];
                var limitDate = params[3];
                
                variables['limitDate'] = limitDate;
                variables['message'] = message;
                variables['product'] = product;
                variables['amount2pay'] = amount2pay;


                var his =  "InvoiceContract created on " + product + " for " + amount2pay ;
                var history = {
                     start: "InvoiceContract created",

                    };
                var variables = {
                     limitDate: limitDate,
                     message: message,
                     product: product,
                     amount2pay: amount2pay
                    };
               // var receiver=params[4];
                // put temp contract reference in db
                var cache = [];
              //  add_contract_db(name,JSON.stringify(abi),'0x0',Math.floor(Date.now() / 1000),accountNum,receiver,cache);
                add_contract_db2(name,JSON.stringify(abi),'0x0',Math.floor(Date.now() / 1000),accountNum,receiver,cache, 1, JSON.stringify(variables), history );
               // add_history_db(name, accountNum, receiver, 1,1, 1, Math.floor(Date.now() / 1000),"Invoice Contract created");
                var contractDeployed = contract.new(
                    amount2pay,
                    product,
                    message,
                    limitDate,
                    {
                        from: self.get_account(accountNum),
                        data: bin.toString(),
                        gas: '5000000'
                    },
                    function(err, contract){
                        if (err) {
                            delete_contract_db(name,JSON.stringify(abi),cache[0]);
                            console.log("ERROR in callback: "+ err);
                            console.log("Contract deployment stopped");
                            return false
                        }
                        else {
                            mine('start', 4);
                            if (typeof contract.address !== 'undefined') {
                                mine('stop');
                                update_contract_db(contract.address,Math.floor(Date.now() / 1000),cache[0])
                                console.log('Contract mined! address: ' + contract.address);
                                console.log('Saving to Database...');

                            }
                        }
                    }
                ); // end contractDeployed
            }
            else if(name === 'EscrowContract'){
                web3.miner.setEtherbase(self.get_account(accountNum));
                var cache = [];
                var message = "Apples ";//params[0];
                var amount2pay = params[1];

                             
                
                 var history = dateform + ": EscrowContract created <br>";
                var variables = {
              
                     message: "Apples",
                    
                     amount2pay: amount2pay
                    };

                console.log(JSON.stringify(variables));
                                console.log(JSON.stringify(history));


                add_contract_db2(name,JSON.stringify(abi),'0x0',Math.floor(Date.now() / 1000),accountNum,receiver,cache, 1,  JSON.stringify(variables), history);
              //  add_history_db(name, accountNum, receiver, 1,1, 1, Math.floor(Date.now() / 1000),"Escrow Contract created: "+amount2pay+" in Escrow");

                var contractDeployed = contract.new(
                    message,
                    {
                        from: self.get_account(accountNum),
                        data: bin.toString(),
                        gas: '5000000'
                    },
                    function(err, contract){
                        if (err) {
                            delete_contract_db(name,JSON.stringify(abi),cache[0]);
                            console.log("ERROR in callback: "+ err);
                            console.log("Contract deployment stopped");
                            return false
                        }
                        else {
                            mine('start', 4);
                            if (typeof contract.address !== 'undefined') {
                                mine('stop');
                                update_contract_db(contract.address,Math.floor(Date.now() / 1000),cache[0])
                                console.log('Contract mined! address: ' + contract.address);
                                console.log('Saving to Database...');
                            }
                        }
                    }
                ); // end contractDeployed
            }
            else{
                console.log('ERROR: Sorry, contract not supported!');
            }
        }); // end fs.readFile
    } // end function


    function initFunctions(){
        // One time run functions.
        // Designed for testing purposes and called only once when new blockchain.

        init_sqlite();
        var accountNum = 0;
        var isMining = false;
        var t = setInterval(function(){
            if (self.get_account(3) == null){
                // Create an account
                if (!isMining){
                    if (self.get_account(accountNum) == null){
                      web3.personal.newAccount('0000');
                    }
                    var wei = parseInt(self.get_wei(self.get_account(accountNum)));
                    if (wei < 500000000000){
                        web3.miner.setEtherbase(self.get_account(accountNum));
                        console.log('You have ' + wei.toString()+' wei in account '+accountNum);
                        console.log('This is not enough, mining begun...');
                        mine('start');
                        isMining = true;

                        var filter = web3.eth.filter('latest');
                        filter.watch(function(err, result){
                            if (err != null){
                                console.log('Error in fitler watch'+ err);
                                mine('stop');
                                console.log('Error when mining account '+accountNum);
                                filter.stopWatching();
                            }
                            if (parseInt(self.get_wei(self.get_account(accountNum))) >= 500000000000){
                                console.log('You now have enough wei on account '+accountNum);
                                mine('stop');
                                isMining = false;
                                filter.stopWatching();
                            }
                        });
                    }
                    else{
                        console.log('Nice, you have ' + wei.toString()+' wei in account '+accountNum);
                        accountNum++;
                    }
                }
            } else {
                console.log('You are ready to play with the Blockchain');
                clearInterval(t);
                return true;
            }
        },2000);

    }
    initFunctions();

    self.contract_call = function(abi, address, func, accountNum){
        // Call contract's functions (getters).
        // If you want to run a contract modifying the state use contract_run
        abi = JSON.parse(abi);
        var contract = web3.eth.contract(abi).at(address);
        return contract[func].call({from:self.get_account(accountNum)});
    }

    self.contract_run = function(abi, address, func, arg = '', payable = 0, cache, accountNum){
        // need to make async function that fires when contract updated
        // FOR THE MOMENT SUPPORTS ONLY 1 ARGUMENT
        abi = JSON.parse(abi);
        web3.personal.unlockAccount(self.get_account(accountNum),'0000');
        web3.miner.setEtherbase(self.get_account(accountNum));
        // console.log('DEBUG: arg inside contract_run ',arg);
        // console.log('DEBUG: payable inside contract_run ',payable);
        if(payable == 0){
            // Execute if we don't want to send money to contracts
            var contract = web3.eth.contract(abi).at(address);
            if (arg === ''){
                contract[func]({from:self.get_account(accountNum),gas:500000},function(err, result){
                    mine('start',4);
                    var filter = web3.eth.filter('latest');
                    filter.watch(function(err, result2){
                        var temp = web3.eth.getTransactionReceipt(result);
                        if (temp != null){
                            mine('stop');
                            filter.stopWatching();
                            cache.push('Contract mined');
                            console.log('DEBUG: contract mined')
                        }
                    });
                });
            }
            else {
                contract[func](arg,{from:self.get_account(accountNum),gas:500000},function(err, result){
                    mine('start',4);
                    var filter = web3.eth.filter('latest');
                    filter.watch(function(err, result2){
                        var temp = web3.eth.getTransactionReceipt(result);
                        if (temp != null){
                            mine('stop');
                            filter.stopWatching();
                            cache.push('Contract mined');
                            console.log('DEBUG: contract mined')
                        }
                    });
                });
            }
        }
        else {
            var contract = web3.eth.contract(abi).at(address);
            if (arg === ''){
                contract[func]({from:self.get_account(accountNum),value:payable, gas:500000},function(err, result){
                    mine('start',4);
                    var filter = web3.eth.filter('latest');
                    filter.watch(function(err, result2){
                        var temp = web3.eth.getTransactionReceipt(result);
                        if (temp != null){
                            mine('stop');
                            filter.stopWatching();
                            cache.push('Contract mined');
                            console.log('DEBUG: contract mined')
                        }
                    });
                });
            }
            else {
                contract[func](arg,{from:self.get_account(accountNum),value:payable, gas:500000},function(err, result){
                    mine('start',4);
                    var filter = web3.eth.filter('latest');
                    filter.watch(function(err, result2){
                        // console.log('result2',result2);
                        var temp = web3.eth.getTransactionReceipt(result);
                        if (temp){
                            mine('stop');
                            filter.stopWatching();
                            cache.push('Contract mined');
                        }
                    });
                });
            }
        }
    }
    return self;
}());

module.exports = gethApi;
