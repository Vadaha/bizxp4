var express = require("express");
var app = express();
var gethApi = require('./gethApi.js');


// Where static files are
app.use(express.static("public"));
// Template engine using
app.set("view engine","ejs");



app.get('/',function(req,res){
    res.render("index");
});

app.get('/read_db',function(req,res){
    var contracts = [];
    gethApi.read_top10_db(contracts);
    var t = setInterval(function(){
        if(contracts.length > 0){
            res.header("Content-Type",'application/json');
            clearInterval(t);
            res.send(JSON.parse(JSON.stringify(contracts)));
        }
    },100)
});
app.get('/read_db_50',function(req,res){
    var contracts = [];
    gethApi.read_top50_db(contracts);
    var t = setInterval(function(){
        if(contracts.length > 0){
            res.header("Content-Type",'application/json');
            clearInterval(t);
            res.send(JSON.parse(JSON.stringify(contracts)));
        }
    },100)
});
app.get('/callContract',function(req,res){
    var data = [];
    var abi = req.query['abi'];
    var address = req.query['address'];
    var func = req.query['func'];
    var account = parseInt(req.query['account']); // integer [0-4]
    console.log('account',account)
    console.log('address',address)
    data = gethApi.contract_call(abi,address,func,account);
    console.log('data:',data)
    res.header("Content-Type",'application/json');
    res.send(data);
});

app.get('/runContract',function(req,res){
    var data = []
    var abi = req.query['abi'];
    var address = req.query['address'];
    var func = req.query['func'];;
    var arg = req.query['arg'];
    var payable = parseInt(req.query['payable']);
    var account = parseInt(req.query['account']);

    gethApi.contract_run(abi,address,func,arg,payable,data,account);
    var t = setInterval(function(){
        if(data[0] != null){
            clearInterval(t);
            res.header("Content-Type",'application/json');
            res.send(data);
        }
    },2000);
});

app.get('/invoice',function(req,res){
    var amount = req.query['amount'];
    var product = req.query['product'];
    var message = req.query['message'];
    var limitDate = req.query['limitDate'];
    var account = parseInt(req.query['account']);
    var receiver = parseInt(req.query['receiver']);

console.log(req.query.receiver);

console.log(receiver)

    gethApi.deploy_sol('InvoiceContract',account,receiver,amount,product,message,limitDate);
    res.redirect('/contracts');
});

app.get('/escrow',function(req,res){
    var message = req.query['message'];
    var account = parseInt(req.query['account']);
    var receiver = parseInt(req.query['receiver']);
    var amount = req.query['amount'];
console.log(req.query.receiver);    
console.log(receiver)
    gethApi.deploy_sol('EscrowContract',account,receiver,message,amount);
    res.redirect('/contracts');
});

app.get('/contracts',function(req,res){
    res.render("contracts");
});

//VG::new api for external users
app.get('/contracts_by_user',function(req,res){
    res.render("contracts_by_user");
});
//

// get an instance of router
var router = express.Router();

// route with parameters (http://localhost:8080/hello/:name)
router.get('/user_:id', function(req, res) {
  
    res.render("user",{id: req.params.id});
    //render
});

// apply the routes to our application
app.use('/', router);


//

app.listen(8080, function(){
    console.log("Listening on port 8080");
});
